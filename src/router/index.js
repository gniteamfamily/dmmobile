import Vue from 'vue';
import VueRouter from 'vue-router';
import Main from '../vue/Main.vue';
import Details from '../vue/Details.vue';

Vue.use(VueRouter);

const routes = [
    { path: '/', name: 'Main', component: Main },
    { path: '/details/:id', name: 'Details', component: Details },
  ];
  
  const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes,
  });

  export default router;